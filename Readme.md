# Flutter App Benchmark

This app was build for better understanding the Flutter workflow.

## VSCode

We recommend using VSCode for this project.

Install [Remote Development extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack).
This way, there will be no need for downloading flutter dependencies and
all your workspace will be entirelly containerized.

## Emulators

### IOS

- XCode

### Android

- Genymotion
